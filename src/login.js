import React, { useState } from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity, Alert } from 'react-native';
import { AntDesign, Feather } from '@expo/vector-icons'; 
import { LinearGradient } from 'expo-linear-gradient';
import { useFonts } from 'expo-font'; 
import rotas from './api'; 

const Login = ({ navigation }) => {
  const [usuario, setUsuario] = useState('');
  const [senha, setSenha] = useState('');
  const [showPassword, setShowPassword] = useState(false);
  const [isPasswordSet, setIsPasswordSet] = useState(false);
  const [loginStatus, setLoginStatus] = useState(null);

  const [loaded] = useFonts({
    poppinsNormal: require('../assets/fonts/poppins/Poppins-Regular.ttf'),
    poppinsBold: require('../assets/fonts/poppins/Poppins-Bold.ttf'),
    poppinsBlack: require('../assets/fonts/poppins/Poppins-Black.ttf'),
    poppinsSemiBold: require('../assets/fonts/poppins/Poppins-SemiBold.ttf'),
    poppinsMedium: require('../assets/fonts/poppins/Poppins-Medium.ttf'),
  });

  if (!loaded) {
    return null; 
  }

  const login = async () => {
    try {
      const response = await rotas.logUser({ email: usuario, senha: senha });

      if (response.status === 200) {
        Alert.alert(
          'Login realizado com sucesso.',
          'Você será redirecionado para outra página',
          [
            { text: 'Ok', onPress: () => navigation.navigate('Pagina1') }
          ]
        );
      } 
    } catch (error) {
      console.error("Erro ao fazer login:", error);
      Alert.alert(
        'Ops, ocorreu um erro:',
        error.response ? error.response.data.error : error.message,
        [
          { text: 'Ok', onPress: () => console.log('OK Pressed') }
        ]
      );
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.titleContainer}>
        <Text style={styles.title}>Faça seu login</Text>
      </View>

      <View style={styles.usuarioContainer}>
        <Text style={styles.senhaText}>Email</Text>
      </View>
      <View style={styles.inputContainer}>
        <AntDesign name="user" size={24} color={'#fff'} style={styles.icon} />
        <TextInput
          style={styles.input}
          value={usuario}
          onChangeText={setUsuario}
        />
      </View>

      <View style={styles.senhaContainer}>
        <Text style={styles.senhaText}>Senha</Text>
      </View>
      <View style={styles.inputContainer}>
        <AntDesign name="lock" size={24} color={'#fff'} style={styles.icon} />
        <TextInput
          style={styles.input}
          secureTextEntry={!showPassword}
          value={senha}
          onChangeText={(text) => {
            setSenha(text);
            setIsPasswordSet(text.length > 0);
          }}
        />
        {isPasswordSet && (
          <Feather
            name={showPassword ? "eye-off" : "eye"} 
            size={24} 
            color="white" 
            onPress={() => setShowPassword(!showPassword)} 
            style={styles.eyeIcon}
          />
        )}
      </View>

      <View style={styles.autenticado}>
        {loginStatus === "success" && <Text style={[styles.statusText, { color: 'green' }]}>Login efetuado com sucesso!</Text>}
        {loginStatus === "failure" && <Text style={[styles.statusText, { color: 'red' }]}>Usuário ou senha incorretos.</Text>}
        <Text style={styles.link}>Esqueceu sua senha?</Text>
      </View>

      <View style={styles.botaoContainer}>
        <TouchableOpacity style={styles.botao} onPress={usuario && senha ? login : null}>
          <LinearGradient
            colors={["#d1f3db", "#7ebea0", "#fef7e1"]}
            style={styles.linearGradient}
            start={{ x: 0, y: 2 }}
            end={{ x: 1, y: 0 }}
          >
            <Text style={styles.botaoText}>Login</Text>
          </LinearGradient>
        </TouchableOpacity>
      </View>
      <View>
        <TouchableOpacity onPress={() => navigation.navigate('Cadastro')}>
          <Text style={styles.register}>Ainda não possui uma conta?</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#171A26',
    opacity: 1
  },
  titleContainer: {
    marginTop: 150,
  },
  title: {
    fontSize: 38,
    fontFamily:'poppinsMedium',
    color: '#7ebea3',
  },
  inputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 50,
    borderBottomWidth: 1,
    borderColor: '#fff', // Alteração da cor da borda
    borderRadius: 5,
    paddingHorizontal: 10,
    
  },
  input: {
    flex: 0.8,
    height: 40,
    color: '#fff',
    fontFamily: 'poppinsMedium'
  },
  icon: {
    marginRight: 5,
  },
  link: {
    marginTop: 10,
    fontSize: 12,
    color: '#fff',
    left:122,
    fontFamily: 'poppinsMedium'
  },
  register: {
    fontSize: 15,
    color: '#22806b',
    marginTop: 30,
    fontFamily: 'poppinsMedium'
  },
  botaoContainer: {
    marginTop: 50,
    
  },
  botao: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
    width: 150,
    borderRadius: 25,
    overflow: 'hidden',
    marginBottom: 10,
    marginTop: 5,
    paddingHorizontal: 12,
    elevation: 10
  },
  botaoText: {
    color: '#fff',
    fontSize: 16,
    fontFamily: 'poppinsMedium'
  },
  linearGradient: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
    width: 150,
    borderRadius: 25,
  },
  usuarioContainer:{
    top:50,
    right:150,
  },
  senhaContainer:{
    top:50,
    right:152
  },
  statusText:{
    fontSize:13,
    position:'absolute',
    right:-27,
    fontFamily: 'poppinsMedium',
    top: 33
  },
  senhaText:{
    fontFamily: 'poppinsMedium',
    marginLeft: 0,
    color: '#fff'
  },
  placeholder: {
    color: 'white'
  }

  
});

export default Login;
