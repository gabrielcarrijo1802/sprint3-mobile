import React, { useState } from 'react';
import { View, Text, StyleSheet, Image, Modal, TextInput, ScrollView, TouchableOpacity } from 'react-native';
import { useFonts } from 'expo-font';
import { AntDesign, Ionicons, FontAwesome6 } from '@expo/vector-icons';
import ModalReserva from '../components/modalreservas';

const HomePage = ({ navigation }) => {
  const [modalVisible, setModalVisible] = useState(false);

  const openModal = () => {
    setModalVisible(true);
  };

  const closeModal = () => {
    setModalVisible(false);
  };

  const perfil = () => {
    navigation.navigate('Perfil');
  };

  const [loaded] = useFonts({
    poppinsNormal: require('../../assets/fonts/poppins/Poppins-Regular.ttf'),
    poppinsBold: require('../../assets/fonts/poppins/Poppins-Bold.ttf'),
    poppinsBlack: require('../../assets/fonts/poppins/Poppins-Black.ttf'),
    poppinsSemiBold: require('../../assets/fonts/poppins/Poppins-SemiBold.ttf'),
    poppinsMedium: require('../../assets/fonts/poppins/Poppins-Medium.ttf'),
    poppinsLight: require('../../assets/fonts/poppins/Poppins-Light.ttf'),
  });

  if (!loaded) {
    return null;
  }

  const images = {
    Paris: require('../../assets/suite8.jpg'),
    Atenas: require('../../assets/suite7.jpeg'),
    Dublin: require('../../assets/suite6.jpg'),
    Londres: require('../../assets/suite5.jpg'),
    Toronto: require('../../assets/suite4.jpg'),
    Sidney: require('../../assets/suite3.jpg'),
    Chicago: require('../../assets/suite2.jpg'),
    Barcelona: require('../../assets/suite.jpg'),
  };

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Text style={styles.headerText}>Seja bem-vindo</Text>
        <TouchableOpacity style={styles.iconWrapper} onPress={perfil}>
          <FontAwesome6 style={styles.userIcon} name="circle-user" size={24} color="white" />
        </TouchableOpacity>
        <Text style={styles.headerSubtitle}>Onde você quer se hospedar hoje?</Text>
      </View>

      <View style={styles.searchContainer}>
        <AntDesign style={styles.searchIcon} name="search1" size={23} color="white" />
        <TextInput style={styles.searchInput} placeholder="Procure o local ideal para você ficar." placeholderTextColor="#999" />
      </View>

      <Text style={styles.sectionTitle}>Explore nossas suítes de luxo:</Text>

      <Modal animationType="slide" transparent visible={modalVisible} onRequestClose={closeModal}>
        <View style={styles.modalContainer}>
          <View style={styles.modalContent}>
            <Text style={styles.modalTitle}>Reservar quarto</Text>
            <ModalReserva />
            <TouchableOpacity style={styles.reserveButton} onPress={closeModal}>
              <Text style={styles.reserveButtonText}>Reserve</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>

      <ScrollView style={styles.suiteScroll} horizontal showsHorizontalScrollIndicator={false}>
        {Object.entries(images).map(([key, imgSrc]) => (
          <TouchableOpacity key={key} style={styles.suiteButton} onPress={openModal}>
            <Image source={imgSrc} style={styles.suiteImage} />
            <Text style={styles.suiteTitle}>Suíte Luxo</Text>
            <Text style={styles.suiteDescription}>
              Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the
              industry's standard dummy text ever since the 1500s.
            </Text>
            <View style={styles.starsContainer}>
              <Ionicons name="star" size={24} color="#ffb300" />
              <Ionicons name="star" size={24} color="#ffb300" />
              <Ionicons name="star" size={24} color="#ffb300" />
              <Ionicons name="star" size={24} color="#ffb300" />
              <Ionicons name="star-half" size={24} color="#ffb300" />
            </View>
            <TouchableOpacity style={styles.reserveSuiteButton} onPress={openModal}>
              <Text style={styles.reserveSuiteButtonText}>Reserve aqui</Text>
            </TouchableOpacity>
          </TouchableOpacity>
        ))}
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#171A26',
  },
  header: {
    marginTop: 20,
    marginLeft: 20,
  },
  headerText: {
    fontSize: 23,
    color: '#fff',
    fontFamily: 'poppinsNormal',
    letterSpacing: 1,
  },
  headerSubtitle: {
    fontSize: 28,
    fontFamily: 'poppinsLight',
    color: '#fff',
  },
  iconWrapper: {
    position: 'absolute',
    right: 20,
    top: 20,
  },
  userIcon: {
    color: '#fff',
  },
  searchContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 10,
    marginHorizontal: 20,
    borderRadius: 20,
    backgroundColor: 'white',
  },
  searchIcon: {
    marginLeft: 15,
  },
  searchInput: {
    flex: 1,
    height: 40,
    paddingLeft: 10,
    color: '#000',
  },
  sectionTitle: {
    fontSize: 20,
    marginTop: 20,
    marginLeft: 20,
    fontFamily: 'poppinsMedium',
    color: 'white',
  },
  suiteScroll: {
    flex: 1,
    marginTop: 20,
  },
  suiteButton: {
    backgroundColor: '#1D2334',
    borderRadius: 10,
    padding: 10,
    margin: 10,
    width: 300,
  },
  suiteImage: {
    width: '100%',
    height: 150,
    borderRadius: 10,
  },
  suiteTitle: {
    fontFamily: 'poppinsLight',
    color: 'white',
    fontSize: 20,
    marginTop: 10,
  },
  suiteDescription: {
    fontFamily: 'poppinsLight',
    color: 'white',
    marginTop: 10,
  },
  starsContainer: {
    flexDirection: 'row',
    marginTop: 10,
  },
  reserveSuiteButton: {
    backgroundColor: '#5D7698',
    borderRadius: 30,
    paddingVertical: 10,
    marginTop: 10,
    alignItems: 'center',
  },
  reserveSuiteButtonText: {
    fontFamily: 'poppinsLight',
    color: 'white',
  },
  modalContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  modalContent: {
    backgroundColor: '#161A27',
    padding: 20,
    borderRadius: 5,
    width: 400,
  },
  modalTitle: {
    fontSize: 20,
    marginBottom: 20,
    fontFamily: 'poppinsLight',
    textAlign: 'center',
    color: 'white',
  },
  modalInput: {
    borderWidth: 1,
    borderColor: 'white',
    marginBottom: 20,
    fontSize: 15,
    fontFamily: 'poppinsLight',
    padding: 10,
    borderRadius: 5,
    backgroundColor: 'white',
    color: 'black',
  },
  reserveButton: {
    backgroundColor: '#5D7698',
    paddingVertical: 10,
    borderRadius: 5,
    alignItems: 'center',
  },
  reserveButtonText: {
    color: 'white',
    fontFamily: 'poppinsLight',
  },
});

export default HomePage;
