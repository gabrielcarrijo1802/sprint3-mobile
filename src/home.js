import { CurrentRenderContext } from '@react-navigation/native';
import React from 'react';
import { View, SafeAreaView, StyleSheet, Platform, StatusBar, Image, Text, TouchableOpacity } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import { useFonts } from 'expo-font'; 



const Home = ({ navigation }) => {
    
    const Barcelona = require('../assets/predio.png')

  const [loaded] = useFonts({
    poppinsNormal: require('../assets/fonts/poppins/Poppins-Regular.ttf'),
    poppinsBold: require('../assets/fonts/poppins/Poppins-Bold.ttf'),
    poppinsBlack: require('../assets/fonts/poppins/Poppins-Black.ttf'),
    poppinsSemiBold: require('../assets/fonts/poppins/Poppins-SemiBold.ttf'),
    poppinsMedium: require('../assets/fonts/poppins/Poppins-Medium.ttf'),
    poppinsLight: require('../assets/fonts/poppins/Poppins-Light.ttf'),
  });

  if (!loaded) {
    return null; 
  }
    
    return (
        <View style={styles.container}>
            <View style={styles.content}>
                <View style={styles.imagemContainer}>
                    <Image
                        source={Barcelona}
                        style={{ width: '100%', height: 1100, width: 500, position: 'absolute', opacity: 1}}
                    />
                    <Text style={styles.title}> Hotel Grand Palace</Text>
                    <Text style={styles.subTitle}> A sua melhor opção, a qualquer momento.</Text>
               
                </View>
                <View style={styles.black}>
                <View style={styles.logins}>
                    <Text style={styles.textProsseguir}>Garanta sua reserva de hotéis e quartos luxuosos para todos os lugares do mundo.</Text>
                    <TouchableOpacity style={styles.botao} onPress={() => { navigation.navigate('Login') }}>
                        <LinearGradient
                            colors={["#1E2334", "#161927", "#1E2334"]}
                            style={styles.linearGradient}
                            start={{ x: 0, y: 2 }}
                            end={{ x: 1, y: 0 }}
                        >
                            <Text style={styles.botaoText}>Entrar</Text>
                        </LinearGradient>
                    </TouchableOpacity>
                </View>
            </View>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    content: {
        flex: 1,
        paddingTop: Platform.OS === 'android' ? StatusBar.currentHeight : 0,
        // Adicione outras estilizações conforme necessário
    },
    imagemContainer: {
        alignItems: 'center',
        

    },
    title: {
        fontSize: 45,
        color: "white",
        fontFamily:'poppinsLight',
        top: 40,

    },
    subTitle: {
        color: 'white',
        fontSize: 19,
        fontFamily:'poppinsLight',
        top: 45
    },
    logins: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 100,
        height: 200
    },
    textProsseguir:{
        fontSize: 23,
        fontFamily:'poppinsLight',
        color:"white",
        marginTop: 90,
        marginLeft: 0,
        textAlign: 'center'
        
    },
    botao: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 60,
        width: 250,
        borderRadius: -50,
        marginBottom: 10,
        elevation: 20,
        marginTop: 40,
        
    },
    botaoText: {
        color: 'white',
        fontSize: 25,
        fontFamily:'poppinsLight',
        letterSpacing: 1
    },
    linearGradient: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 60,
        width: 250,
        borderRadius: 30,
        opacity: 0.8
    },
    black: {
        marginTop: 25,
        width: 485,
        height: 260,
        borderRadius: 30,
        borderColor: 'white',
      
        // elevation: 30,
        // paddingStart: 30,
        // paddingEnd: 30,
        opacity: 1
  
   
   
    }
});

export default Home;
