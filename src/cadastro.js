import React, { useState } from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity, Alert } from 'react-native';
import { AntDesign, Feather, Fontisto  } from '@expo/vector-icons'; // Importando ícones do pacote expo
import { LinearGradient } from 'expo-linear-gradient';
import { useFonts } from 'expo-font'; 
import rotas from './api';

const Cadastro = ({ navigation }) => {
  const [usuario, setUsername] = useState('');
  const [email, setEmail] = useState('');
  const [senha, setPassword] = useState('');
  const [cep, setCep] = useState('');
  const [cpf, setCpf] = useState('');
  const [telefone, setTelefone] = useState('');
  const [confirmSenha, setConfirmPassword] = useState('');
  const [showPassword, setShowPassword] = useState(false); // Estado para controlar a visibilidade da senha
  const [showConfirmPassword, setShowConfirmPassword] = useState(false); // Estado para controlar a visibilidade da confirmação de senha
  const [isPasswordSet, setIsPasswordSet] = useState(false); // Estado para controlar se a senha foi definida
  const [isConfirmPasswordSet, setIsConfirmPasswordSet] = useState(false); // Estado para controlar se a senha foi definida
  const [CadastroStatus, setCadastroStatus] = useState(null); // Estado para controlar o status do login


  // Carregar a fonte
  const [loaded] = useFonts({
    poppinsNormal: require('../assets/fonts/poppins/Poppins-Regular.ttf'),
    poppinsBold: require('../assets/fonts/poppins/Poppins-Bold.ttf'),
    poppinsBlack: require('../assets/fonts/poppins/Poppins-Black.ttf'),
    poppinsSemiBold: require('../assets/fonts/poppins/Poppins-SemiBold.ttf'),
    poppinsMedium: require('../assets/fonts/poppins/Poppins-Medium.ttf'),
  });

  if (!loaded) {
    return null; 
  }

 // Função para realizar o cadastro
const Cadastro = async () => {
  try {
    // Chama a função signInUser da API para fazer o cadastro
    const response = await rotas.signInUser({ usuario, senha, confirmSenha, email, cpf, cep, telefone });

    // Se a resposta da API for bem-sucedida (status 200), exibe um alerta de sucesso
    if (response.status === 200) {
      Alert.alert(
        'Bem-vindo ao Hotel Grand Palace.', // Título do alerta de sucesso
        'Você será redirecionado para outra página', // Mensagem do alerta de sucesso
        [
          { text: 'Ok', onPress: () => navigation.navigate('Pagina1') } // Botão "Ok" para redirecionar para outra página
        ]
      );
    }
  } catch (error) {
    // Se ocorrer um erro durante o cadastro, captura o erro e exibe um alerta com a mensagem de erro
    console.log("Erro ao fazer cadastro:", error);
    Alert.alert(
      'Ops, ocorreu um erro:', // Título do alerta de erro
      error.response.data.message, // Mensagem de erro retornada pela API
      [
        { text: 'Ok', onPress: () => console.log('OK Pressed') } // Botão "Ok" para fechar o alerta de erro
      ]
    );
  }
};

  
  

  return (
    <View style={styles.container}>
      <View style={styles.titleContainer}>
        <Text style={styles.title}>Cadastre-se</Text>
      </View>
      <View style={styles.usuarioContainer}>
        <Text style={styles.senhaText}>
          Usuário
        </Text>
      </View>
      <View style={styles.inputContainer}>
        <AntDesign name="user" size={24} color={'white'} style={styles.icon} />
        <TextInput
          style={styles.input}
        
          value={usuario}
          onChangeText={setUsername}
        />
      </View>

      


      <View style={styles.emailContainer}>
        <Text style={styles.senhaText}>
          E-mail
        </Text>
      </View>
      <View style={styles.inputContainer}>
      <Fontisto name="email" size={22} color="white" style={styles.icon} />
        <TextInput
          style={styles.input}
      
          value={email}
          onChangeText={setEmail}
        />
      </View>

      <View style={styles.usuariowContainer}>
        <Text style={styles.senhaText}>
          Cpf
        </Text>
      </View>
      <View style={styles.inputContainer}>
        <AntDesign name="user" size={24} color={'white'} style={styles.icon} />
        <TextInput
          style={styles.input}
        
          value={cpf}
          onChangeText={setCpf}
        />
      </View>

      <View style={styles.usuariowContainer}>
        <Text style={styles.senhaText}>
          Cep
        </Text>
      </View>
      <View style={styles.inputContainer}>
        <AntDesign name="user" size={24} color={'white'} style={styles.icon} />
        <TextInput
          style={styles.input}
        
          value={cep}
          onChangeText={setCep}
        />
      </View>

      <View style={styles.emailContainer}>
        <Text style={styles.senhaText}>
         Telefone
        </Text>
      </View>
      <View style={styles.inputContainer}>
      <Fontisto name="email" size={22} color="white" style={styles.icon} />
        <TextInput
          style={styles.input}
      
          value={telefone}
          onChangeText={setTelefone}
        />
      </View>

      <View style={styles.senhaContainer}>
        <Text style={styles.senhaText}>
          Senha
        </Text>
      </View>
      <View style={styles.inputContainer}>
  <AntDesign name="lock" size={24} color={'white'} style={styles.icon} />
  <TextInput
    style={styles.input}

    secureTextEntry={!showPassword}
    value={senha}
    onChangeText={(text) => {
      setPassword(text);
      setIsPasswordSet(text.length > 0); // Define como true se o comprimento do texto for maior que 0
    }}
  />
  {isPasswordSet && ( // Renderiza o ícone do olho apenas quando a senha não está vazia
    <Feather 
      name={showPassword ? "eye-off" : "eye"} 
      size={24} 
      color="white" 
      onPress={() => setShowPassword(!showPassword)} 
      style={styles.eyeIcon}
    />
  )}
</View>
<View style={styles.confirmPassword}>
  <Text style={styles.senhaText}>
    Confirmar Senha
  </Text>
</View>
<View style={styles.inputContainer}>
  <AntDesign name="lock" size={24} color={'white'} style={styles.icon} />
  <TextInput
    style={styles.input}
  
    secureTextEntry={!showConfirmPassword}
    value={confirmSenha}
    onChangeText={(text) => {
      setConfirmPassword(text);
      setIsConfirmPasswordSet(text.length > 0); 
    }}
  />
  {isConfirmPasswordSet && ( // Renderiza o ícone do olho apenas quando a senha não está vazia
    <Feather 
      name={showConfirmPassword ? "eye-off" : "eye"} 
      size={24} 
      color="white" 
      onPress={() => setShowConfirmPassword(!showConfirmPassword)} 
      style={styles.eyeIcon}
    />
  )}
</View>

      <View style={styles.autenticado}>
      {CadastroStatus === "success" && <Text style={[styles.statusText, { color: 'green' }]}>Usuário cadastrado com sucesso.</Text>}
      {CadastroStatus === "failure" && <Text style={[styles.statusText, { color: 'red' }]}>Erro no cadastro.</Text>}
     
      </View>
     
      <View style={styles.botaoContainer}>
        <TouchableOpacity style={styles.botao} onPress={Cadastro}>
          <LinearGradient
            colors={["#d1f3db", "#7ebea3", "#fef7e1"]}
            style={styles.linearGradient}
            start={{ x: 0, y: 2 }}
            end={{ x: 1, y: 0 }}
          >
            <Text style={styles.botaoText}>Cadastrar</Text>
          </LinearGradient>
        </TouchableOpacity>
      </View>
      <View>
        <TouchableOpacity onPress={() => navigation.navigate('Login')}>
          <Text style={styles.register}>Já possui uma conta?</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#171A26',
  },
  titleContainer: {
    marginTop: 150,
  },
  title: {
    fontSize: 40,
    fontFamily:'poppinsMedium',
    color: '#7ebea3',
  },
  inputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderColor: '#fff', // Alteração da cor da borda
    borderRadius: 5,
    paddingHorizontal: 10,
    marginTop:10,
    color: '#fff'
  },
  input: {
    flex: 0.8,
    height: 40,
    color: '#fff', // Cor do texto,
    fontFamily: 'poppinsMedium'
  },
  icon: {
    marginRight: 5,
  },
  link: {
    fontSize: 12,
    color: '#000',
    left:90,
  },
  register: {
    fontSize: 15,
    color: '#22806b',
    marginTop: 10,
    fontFamily: 'poppinsMedium'
  },
  botaoContainer: {
    marginTop: 20,
  },
  botao: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
    width: 150,
    borderRadius: 25,
    overflow: 'hidden',
    marginBottom: 10,
    marginTop: 20,
    paddingHorizontal: 12,
    elevation: 10
  },
  botaoText: {
    color: '#fff',
    fontSize: 16,
    fontFamily: 'poppinsMedium'
  },
  linearGradient: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
    width: 150,
    borderRadius: 25,
    elevation: 10
  },
  usuarioContainer:{
    top:10,
    right:150,
  },

  usuariowContainer:{
    top: 10,
    right:160,
  },
  emailContainer:{
    top:10,
    right:150,
  },
  senhaContainer:{
    top:15,
    right:152
  },
  confirmPassword:{
    top:12,
    right:119
  },
  statusText:{
    fontSize:15,
    marginTop: 15,
    fontFamily: 'poppinsMedium'

  },
  senhaText:{
    fontFamily: 'poppinsMedium',
    marginLeft: -40,
    color: '#fff'
  }
  
  
});

export default Cadastro;
