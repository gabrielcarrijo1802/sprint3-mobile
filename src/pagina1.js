import React from 'react';
import { View, StyleSheet, Platform, StatusBar } from 'react-native'; // Importando Platform e StatusBar
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { FontAwesome, MaterialIcons } from '@expo/vector-icons';
import { useFonts } from 'expo-font'; 
import { SimpleLineIcons } from '@expo/vector-icons';

import HomeScreen from './tabScreens/homeScreen';
import ProfileScreen from './tabScreens/profileScreen';
import Header from './header';

const Tab = createBottomTabNavigator();




const BottomTabNavigator = () => {

    

  const [loaded] = useFonts({
    poppinsNormal: require('../assets/fonts/poppins/Poppins-Regular.ttf'),
    poppinsBold: require('../assets/fonts/poppins/Poppins-Bold.ttf'),
    poppinsBlack: require('../assets/fonts/poppins/Poppins-Black.ttf'),
    poppinsSemiBold: require('../assets/fonts/poppins/Poppins-SemiBold.ttf'),
    poppinsMedium: require('../assets/fonts/poppins/Poppins-Medium.ttf'),
    poppinsLight: require('../assets/fonts/poppins/Poppins-Light.ttf'),
  });

  if (!loaded) {
    return null; 
  }






  return (
    <View style={styles.container}>
      <View style={styles.header}>

      </View>

      <View style={styles.tabNavigator}>
        <Tab.Navigator
          screenOptions={({ route }) => ({
            headerShown: false,


            tabBarStyle:{
              borderTopWidth: 0,
              backgroundColor: '#1F2733',
              
            
            },

            tabBarIcon: ({ color, size }) => {
              let iconName;

              if (route.name === 'Reservas') {
                iconName = 'calendar';
                return <SimpleLineIcons name={iconName} size={size} color={color} />
              } else if (route.name === 'Search') {
                iconName = 'search';
                return <FontAwesome name={iconName} size={size} color={color} />;
              } else if (route.name === 'Perfil') {
                iconName = 'account-circle';
                return <MaterialIcons name={iconName} size={size} color={color} />;
              }
            },
            tabBarActiveTintColor: '#f9fad2',
            tabBarInactiveTintColor: 'white',
            tabBarLabelStyle: {
              fontFamily: 'poppinsLight', // Definindo a fonte Poppins
              fontSize: 12, // Defina o tamanho da fonte desejado
            },

          })}
        >
          <Tab.Screen name="Reservas" component={HomeScreen} />

          <Tab.Screen name="Perfil" component={ProfileScreen} />
        </Tab.Navigator>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    paddingTop: Platform.OS === 'android' ? StatusBar.currentHeight : 0,
  },
  tabNavigator: {
    flex: 1,
    position: 'relative',
    left: 0,
    borderRadius: 10,
    
    
  },
});

export default BottomTabNavigator;
