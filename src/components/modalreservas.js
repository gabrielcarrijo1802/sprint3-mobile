import React, { useState } from "react";
import { View } from "react-native";
import DateTimePicket from "./datePicker";
import { Colors } from "react-native/Libraries/NewAppScreen";


const modalReserva = () => {
    const [reserva, setReserva] = useState({
        tipo_quarto: null,
        numero_quarto: null,
        Data_de_entrada: 'Data de entrada',
        Data_de_saida: 'Data de saída',
    });

    return (
        <View>
            <DateTimePicket type={'date'} buttonTitle={reserva.Data_de_entrada} dateKey={'Data_de_entrada'} setReserva={setReserva} />
            <DateTimePicket type={'date'} buttonTitle={reserva.Data_de_saida} dateKey={'Data_de_saida'} setReserva={setReserva} />
        </View>
    );
}

export default modalReserva;