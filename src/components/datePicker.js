import React, { useState } from "react";
import { View, Button } from "react-native";
import DateTimePickerModal from "react-native-modal-datetime-picker";

const SelectDateTime = ({ type, buttonTitle, dateKey, setSchedule }) => {
  const [datePickerVisible, setDatePickerVisible] = useState(false);

  const showDatePicker = () => {
    setDatePickerVisible(true);
  };

  const hideDatePicker = () => {
    setDatePickerVisible(false);
  };

  const handleConfirm = (date) => {
    if (type === "time") {
      // Logica para extrair hora e minuto  
      const hour = date.getHours();
      const minute = date.getMinutes();

      setSchedule((prevState) => ({
        ...prevState,
        [dateKey]: formattedTime,
      }));
    } else {
      const formattedTime = date.toISOString().split("T")[0];
      //Atualiza Schedule
      setSchedule((prevState) => ({
        ...prevState,
        [dateKey]: formattedTime,
      }));
    }
  };

  return (
    <View>
      <Button title={buttonTitle} onPress={showDatePicker} color="#000" />
      <DateTimePickerModal
        isVisible={datePickerVisible}
        mode={type}
        locale="pt_BR"
        onConfirm={handleConfirm}
        onCancel={hideDatePicker}
        textColor="#000"
      />
    </View>
  );
};

export default SelectDateTime;
